This small Python tool converts video between the standard VP8 IVF format (https://wiki.multimedia.cx/index.php/IVF) and the ca_vp8 format used in Creative Assembly's 2015 video game Total War: Attila (and also by later games from the same developer).

This is an early proof of concept, and while it should work for most cases, is not by any means trivial to use. You must encode your videos to VP8 IVF using some other tool such as FFmpeg, or the reference 'vpxenc.exe' encoder from the WebM project (https://www.webmproject.org/).

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

*Encoding video:*

```
ffmpeg -i 'example.mkv' -c:v libvpx -crf 4 -b:v 10M 'front_end_background.ivf'
python convertCAMV.py 'front_end_background.ivf' 'front_end_background.ca_vp8'
```

*Decoding video:*

```
python convertCAMV.py 'intro.ca_vp8' 'intro.ivf'
ffmpeg -i 'intro.ivf' -c:v libx264 -preset slow -crf 22 'intro.mkv'
```