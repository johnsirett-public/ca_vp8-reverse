from io import SEEK_CUR
from collections import namedtuple
from fractions import Fraction
from struct import unpack, pack
import argparse

CAMVFrameEntry = namedtuple('CAMVFrameEntry', 'frameOffset frameSize frameFlags')
IVFFrameEntry = namedtuple('IVFFrameEntry', 'frameOffset frameSize framePTS')

def camv_to_ivf(inFile, outFile):
    # Setup demxuing contexts.
    camv = {}
        
    # Read CAMV header.
    camv['signature'] = inFile.read(4)
    camv['version'] = int.from_bytes(inFile.read(2), 'little')
    camv['headerLen'] = int.from_bytes(inFile.read(2), 'little')
    camv['codecFourCC'] = inFile.read(4)
    camv['width'] = int.from_bytes(inFile.read(2), 'little')
    camv['height'] = int.from_bytes(inFile.read(2), 'little')
    camv['msPerFrame'] = unpack('<f', inFile.read(4))[0]
    inFile.read(4) # always 1, not sure what it encodes
    camv['numFrames'] = int.from_bytes(inFile.read(4), 'little') + 1
    offsetFrameTable = int.from_bytes(inFile.read(4), 'little')
    inFile.read(4) # unneeded copy of numFrames
    camv['largestFrame'] = int.from_bytes(inFile.read(4), 'little')

    # Read frame offsets / sizes.
    frameTable = []
    inFile.seek(offsetFrameTable)
    for i in range(camv['numFrames']):
        frameOffset = int.from_bytes(inFile.read(4), 'little')
        frameSize = int.from_bytes(inFile.read(4), 'little')
        frameFlags = int.from_bytes(inFile.read(1), 'little')
        frameTable.append(CAMVFrameEntry(frameOffset, frameSize, frameFlags))
    camv['frameTable'] = frameTable

    # Calculate new timebase.
    framerate = Fraction(1000 / camv['msPerFrame'])
    timebase = Fraction(framerate.denominator, framerate.numerator)
    timebase = timebase.limit_denominator(10000)

    # Write IVF header.
    outFile.write(b'DKIF')                       # signature
    outFile.write(int.to_bytes(0, 2, 'little'))  # version
    outFile.write(int.to_bytes(32, 2, 'little')) # header length
    outFile.write(camv['codecFourCC'])           # codec FourCC
    outFile.write(int.to_bytes(camv['width'], 2, 'little'))  # width
    outFile.write(int.to_bytes(camv['height'], 2, 'little')) # height
    outFile.write(int.to_bytes(timebase.denominator, 4, 'little'))  # timebase denom
    outFile.write(int.to_bytes(timebase.numerator, 4, 'little'))  # timebase numer
    outFile.write(int.to_bytes(camv['numFrames'], 4, 'little')) # framecount
    outFile.write(b'\x00\x00\x00\x00')           # unused bytes

    # Write IVF frames.
    for i, frameEntry in enumerate(camv['frameTable']):
        # Read the CAMV frame.
        inFile.seek(frameEntry.frameOffset)
        frameData = inFile.read(frameEntry.frameSize)

        # Write out the IVF frame.
        outFile.write(int.to_bytes(len(frameData), 4, 'little')) # size of frame
        # FIXME: this can't be the right PTS logic
        outFile.write(int.to_bytes(i, 8, 'little')) # PTS
        outFile.write(frameData)

    # Close files.
    inFile.close()
    outFile.close()

def ivf_to_camv(inFile, outFile):
    # Setup demxuing contexts.
    ivf = {}

    # Read IVF header
    ivf['signature'] = inFile.read(4)
    ivf['version'] = int.from_bytes(inFile.read(2), 'little')
    ivf['headerLen'] = int.from_bytes(inFile.read(2), 'little')
    ivf['codecFourCC'] = inFile.read(4)
    ivf['width'] = int.from_bytes(inFile.read(2), 'little')
    ivf['height'] = int.from_bytes(inFile.read(2), 'little')
    ivf['timebaseDenom'] = int.from_bytes(inFile.read(4), 'little')
    ivf['timebaseNumer'] = int.from_bytes(inFile.read(4), 'little')
    ivf['numFrames'] = int.from_bytes(inFile.read(4), 'little')
    inFile.read(4) # unused bytes

    # Read IVF frame offsets / sizes.
    frameTable = []
    for _ in range(ivf['numFrames']):
        frameOffset = inFile.tell() + 12
        frameSize = int.from_bytes(inFile.read(4), 'little')
        framePTS = int.from_bytes(inFile.read(8), 'little')
        frameTable.append(IVFFrameEntry(frameOffset, frameSize, framePTS))

        inFile.seek(frameSize, SEEK_CUR)
    ivf['frameTable'] = frameTable

    # Calculate new millisecond per frame.
    framerate = ivf['timebaseDenom'] / ivf['timebaseNumer']
    msPerFrame = 1000 / framerate

    # Write early part of CAMV header.
    outFile.write(b'CAMV') # signature
    outFile.write(int.to_bytes(0, 2, 'little'))  # version
    outFile.write(int.to_bytes(32, 2, 'little')) # header length
    outFile.write(ivf['codecFourCC'])            # codec FourCC
    outFile.write(int.to_bytes(ivf['width'], 2, 'little'))  # width
    outFile.write(int.to_bytes(ivf['height'], 2, 'little')) # height
    outFile.write(pack('<f', msPerFrame))        # milliseconds per frame
    outFile.write(int.to_bytes(1, 4, 'little'))  # unknown field, always 1
    outFile.write(int.to_bytes(ivf['numFrames'] - 1, 4, 'little'))
    outFile.seek(12, SEEK_CUR) # come back to write frameTable-dependent fields

    # Write out CAMV frames.
    camvFrameTable = []
    for frameEntry in ivf['frameTable']:
        # Read the IVF frame.
        inFile.seek(frameEntry.frameOffset)
        frameData = inFile.read(frameEntry.frameSize)

        # Determine keyframe.
        keyframe = False
        if frameData[3:6] == b'\x9D\x01\x2A':
            keyframe = True

        # Create the entry for the CAMV frameTable.
        frameOffset = outFile.tell()
        frameSize = frameEntry.frameSize
        frameFlags = 1 if keyframe else 0
        camvFrameTable.append(CAMVFrameEntry(frameOffset, frameSize, frameFlags))

        # Write out the CAMV frame.
        outFile.write(frameData)

    # Write out the CAMV frame table.
    offsetFrameTable = outFile.tell()
    lenFrameTable = len(camvFrameTable)
    largestFrame = max(x.frameSize for x in camvFrameTable)
    for frameEntry in camvFrameTable:
        outFile.write(int.to_bytes(frameEntry.frameOffset, 4, 'little'))
        outFile.write(int.to_bytes(frameEntry.frameSize, 4, 'little'))
        outFile.write((int.to_bytes(frameEntry.frameFlags, 1, 'little')))

    # Write final parts of CAMV header.
    outFile.seek(0x1c)
    outFile.write(int.to_bytes(offsetFrameTable, 4, 'little')) # offset of frame table
    outFile.write(int.to_bytes(lenFrameTable, 4, 'little'))    # length of frame table
    outFile.write(int.to_bytes(largestFrame, 4, 'little'))     # size of largest frame

    # Close files.
    inFile.close()
    outFile.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Converts between VP8 IVF (.ivf) and Creative Assembly CAMV (.ca_vp8) files.')
    parser.add_argument('inFile', type=argparse.FileType('rb'))
    parser.add_argument('outFile', type=argparse.FileType('wb'))
    args = parser.parse_args()

    magic = args.inFile.read(4)
    args.inFile.seek(0)
    if magic == b'DKIF':
        ivf_to_camv(args.inFile, args.outFile)
    elif magic == b'CAMV':
        camv_to_ivf(args.inFile, args.outFile)
