# Overall notes

- Some kind of modified VP8 IVF?
- End of the file contains a table of frame offsets and size, pointed to by the modified header.

# Headers (Frame/File, don't know which yet)

- 0x00-0x03: signature 'CAMV'
- 0x04-0x05: version (always 0)
- 0x06-0x07: length of file header (??claims 32 but is it really??)
- 0x08-0x0B: codec FourCC, 'VP80'
- 0x0C-0x0D: width of video
- 0x0E-0x0F: height of video
- 0x10-0x13: milliseconds per frame (float32)
- 0x14-0x17: ??? not sure what it codes but it's always 1 ???
- 0x18-0x1B: number of frames - 1
- 0x1C-0x1F: file offset to frame offsets table
- 0x20-0x23: length of frame offsets table in entries (so basically number of frames again)
- 0x24-0x27: size of largest frame's data (buffer allocation size)

# Frame Offsets / Sizes table

Pointed to by 0x1C-0x1F in the main header. Codes exactly 9 bytes for each frame, which appear to be;

- 4 bytes: file offset of frame data
- 4 bytes: size in bytes of frame data
- 1 byte: keyframe flag: 0x1 for keyframes, 0x0 otherwise